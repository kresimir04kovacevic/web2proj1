"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var https_1 = __importDefault(require("https"));
var express_openid_connect_1 = require("express-openid-connect");
var dotenv_1 = __importDefault(require("dotenv"));
dotenv_1["default"].config();
var app = (0, express_1["default"])();
app.set("views", path_1["default"].join(__dirname, "views"));
app.set('view engine', 'pug');
var port = 4080;
var admins = ["ADMIN"];
var config = {
    authRequired: false,
    idpLogout: true,
    secret: process.env.SECRET,
    baseURL: "https://localhost:".concat(port),
    clientID: 'GdohVAAmkHY7Qfg9g0sH1SkTHWIh95tp',
    issuerBaseURL: 'https://dev-c7pibxseu5z2fg0n.us.auth0.com',
    clientSecret: 'lt0V2OzMEs-oQg5NUaCGtD7IM0KkjuXsbOk4kz5dXVnGBFDV_ipXZr6KuXybI1dA',
    authorizationParams: {
        response_type: 'code'
    }
};
// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use((0, express_openid_connect_1.auth)(config));
app.get('/', function (req, res) {
    var _a, _b, _c;
    var username;
    if (req.oidc.isAuthenticated()) {
        username = (_b = (_a = req.oidc.user) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : (_c = req.oidc.user) === null || _c === void 0 ? void 0 : _c.sub;
    }
    res.render('index', { username: username });
});
app.get('/private', (0, express_openid_connect_1.requiresAuth)(), function (req, res) {
    var _a, _b, _c;
    var user = JSON.stringify(req.oidc.user);
    var username = (_b = (_a = req.oidc.user) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : (_c = req.oidc.user) === null || _c === void 0 ? void 0 : _c.sub;
    for (var _i = 0, admins_1 = admins; _i < admins_1.length; _i++) {
        var admin = admins_1[_i];
        if (username === admin) {
            res.render('private', { user: user, admin: admin, username: username });
        }
    }
    res.render('private', { user: user, username: username });
});
app.get("/sign-up", function (req, res) {
    res.oidc.login({
        returnTo: '/'
    });
});
app.get('/profile', function (req, res) {
    var _a, _b, _c;
    //res.send(JSON.stringify(req.oidc.user));
    if (req.oidc.isAuthenticated()) {
        var user = JSON.stringify(req.oidc.user);
        var username = (_b = (_a = req.oidc.user) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : (_c = req.oidc.user) === null || _c === void 0 ? void 0 : _c.sub;
        for (var _i = 0, admins_2 = admins; _i < admins_2.length; _i++) {
            var admin = admins_2[_i];
            if (username === admin) {
                res.render('profile', { user: user, admin: admin, username: username });
            }
        }
        res.render('profile', { user: user, username: username });
    }
    else {
        res.render('profile');
    }
});
https_1["default"].createServer({
    key: fs_1["default"].readFileSync('server.key'),
    cert: fs_1["default"].readFileSync('server.cert')
}, app)
    .listen(port, function () {
    console.log("Server running at https://localhost:".concat(port, "/"));
});
